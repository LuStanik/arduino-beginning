void setup() {
  // put your setup code here, to run once:
  Serial.begin(9600);

  pinMode(14, OUTPUT);
  pinMode(15, OUTPUT);
  pinMode(16, INPUT_PULLUP);

  digitalWrite(14, LOW);
  digitalWrite(15, LOW);
}

void loop() {
  // put your main code here, to run repeatedly:
  if(digitalRead(16) == LOW)
  {
    digitalWrite(14, HIGH);
    digitalWrite(15, LOW);
  }
  else
  {
    digitalWrite(14, LOW);
    digitalWrite(15, HIGH);
    Serial.println("Bla bla bla");
  }
}
