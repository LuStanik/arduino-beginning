void setup() {
  pinMode(14,OUTPUT);
  pinMode(15,OUTPUT);
  pinMode(16,INPUT_PULLUP);
  digitalWrite(14, LOW);
  digitalWrite(15,HIGH);
  // put your setup code here, to run once:
}

void loop() {
  // put your main code here, to run repeatedly:
  if(digitalRead(16) == LOW) 
  {
    digitalWrite(14,HIGH);
    digitalWrite(15,LOW);
    delay(3000);
    digitalWrite(14,LOW);
    digitalWrite(15,HIGH);
  }
}
