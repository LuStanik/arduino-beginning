void setup() {
  // put your setup code here, to run once:
  pinMode(14, OUTPUT);
  pinMode(15, OUTPUT);
  pinMode(17, OUTPUT);
  pinMode(16, INPUT_PULLUP);
  digitalWrite(14, LOW);
  digitalWrite(15, HIGH);
  digitalWrite(17, LOW);
}

void loop() {
  // put your main code here, to run repeatedly:
  if (digitalRead(16) == LOW)
  {
    digitalWrite(17, HIGH);
    digitalWrite(15, LOW);
    digitalWrite(14, LOW);
    delay(2000);
    digitalWrite(17, LOW);
    digitalWrite(15, LOW);
    digitalWrite(14, HIGH);
    delay(5000);
    digitalWrite(17, HIGH);
    digitalWrite(15, LOW);
    digitalWrite(14, LOW);
    delay(2000);
    digitalWrite(17, LOW);
    digitalWrite(14, LOW);
    digitalWrite(15, HIGH);          
  }
}
