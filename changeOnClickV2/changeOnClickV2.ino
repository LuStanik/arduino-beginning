
void setup() {
  pinMode(15, OUTPUT); //Dioda czerwona
  pinMode(17, OUTPUT); //Dioda żółta
  pinMode(14, OUTPUT); //Dioda zielona
 
  pinMode(16, INPUT_PULLUP); //Przycisk
 
  digitalWrite(15, LOW); //Wyłączenie diod
  digitalWrite(17, LOW);
  digitalWrite(14, LOW);
}
 
void loop()
{
  digitalWrite(15, LOW); //Czerwona
  digitalWrite(14, LOW); //Pomarańczowa
  digitalWrite(17, HIGH); //Zielona
  
  while (digitalRead(16) == HIGH) {} //Czekaj na wciśnięcie przycisku
  
  digitalWrite(15, LOW); //Czerwona
  digitalWrite(14, HIGH); //Pomarańczowa
  digitalWrite(17, LOW); //Zielona
  
  while (digitalRead(16) == HIGH) {} //Czekaj na wciśnięcie przycisku
  
  digitalWrite(15, HIGH); //Czerwona
  digitalWrite(14, LOW); //Pomarańczowa
  digitalWrite(17, LOW); //Zielona
  
  while (digitalRead(16) == HIGH) {} //Czekaj na wciśnięcie przycisku
  
  digitalWrite(15, HIGH); //Czerwona
  digitalWrite(14, HIGH); //Pomarańczowa
  digitalWrite(17, LOW); //Zielona
  
  while (digitalRead(16) == HIGH) {} //Czekaj na wciśnięcie przycisku
}
