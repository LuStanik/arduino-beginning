int readVal = 0;

void setup() {
  // put your setup code here, to run once:
  Serial.begin(9600);
}

void loop() {
  // put your main code here, to run repeatedly:
  readVal = analogRead(A0);
  int readValInVolts = readVal/1024.0 * 5.0;
  Serial.println(readValInVolts);
  delay(200);
}
