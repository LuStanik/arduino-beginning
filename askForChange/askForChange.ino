#define green 15
#define red 14

String receivedData = "";

void setup() {
  // put your setup code here, to run once:
  Serial.begin(9600);
  
  pinMode(green, OUTPUT);
  pinMode(red, OUTPUT);
  
  digitalWrite(green, LOW);
  digitalWrite(red, LOW);
}

void loop() {
  // put your main code here, to run repeatedly:
  if(Serial.available() > 0) 
  {
    receivedData = Serial.readStringUntil('\n');
    if(receivedData == "green")
    {
      digitalWrite(green, HIGH);
      delay(2000);
      digitalWrite(green, LOW);
      receivedData = "";
    }
    if(receivedData == "red")
    {
      digitalWrite(red, HIGH);
      delay(2000);
      digitalWrite(red, LOW);
      receivedData = "";
    }
    if((receivedData != "") && (receivedData != "green") && (receivedData != "red")){ Serial.println("Wrong color, choose either red or green!");}
  }
}
