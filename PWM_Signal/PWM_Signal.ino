#define diode 3

int fill = 0;
int change = 5;

void setup() {
  // put your setup code here, to run once:
  pinMode(diode, OUTPUT);
}

void loop() {
  // put your main code here, to run repeatedly:
  analogWrite(diode, fill);
  if(fill < 255)
  {
    fill = fill+change;
  }
  else
  {
    fill = 0;
  }
  delay(50);
}
