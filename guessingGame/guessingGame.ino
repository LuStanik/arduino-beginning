#define red A4
#define yellow A3
#define green A2

int readVal = 0;
int marginOfError = 100;
int tries = 3;
int playerVal;

void setup() {
  // put your setup code here, to run once:
  pinMode(red, OUTPUT);
  pinMode(yellow, OUTPUT);
  pinMode(green, OUTPUT);
  digitalWrite(red, LOW);
  digitalWrite(yellow, LOW);
  digitalWrite(green, LOW);
  Serial.begin(9600);
  Serial.println("Zgadnij obecna wartosc potencjometru");  
}

void loop() {
  // put your main code here, to run repeatedly:
  readVal = analogRead(A5);
  if(Serial.available() > 0)
  {
    playerVal = Serial.parseInt();
    if((playerVal < readVal + marginOfError) && (playerVal > readVal - marginOfError))
    {
      Serial.println("Great work, keep it up!");
      digitalWrite(green, HIGH);
      digitalWrite(red, LOW);
      digitalWrite(yellow, LOW);
    }
    else if((tries > 0 && playerVal!=-1) && (playerVal >= readVal + marginOfError) && (playerVal <= readVal - marginOfError))
    {
      Serial.println("Theres always another chance :)");
      tries--;
      digitalWrite(green, LOW);
      digitalWrite(red, LOW);
      digitalWrite(yellow, HIGH);
      delay(2000);
      digitalWrite(yellow, LOW);
      playerVal = -1;
    }
    else
    {
      Serial.println("Ok now you lost");
      digitalWrite(green, LOW);
      digitalWrite(yellow, LOW);
      digitalWrite(red, HIGH);
    }
  }
}
