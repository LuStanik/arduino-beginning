void setup() {
  // put your setup code here, to run once:
  pinMode(14,OUTPUT);
  pinMode(15,OUTPUT);
}

void loop() {
  // put your main code here, to run repeatedly:
  digitalWrite(14,HIGH);
  digitalWrite(15,LOW);
  delay(100);
  digitalWrite(14,LOW);
  digitalWrite(15,HIGH);
  delay(100);
}
