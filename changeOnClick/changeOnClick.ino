void setup() {
  // put your setup code here, to run once:
  pinMode(14, OUTPUT);
  pinMode(15, OUTPUT);
  pinMode(17, OUTPUT);
  pinMode(16, INPUT_PULLUP);
  digitalWrite(14, LOW);
  digitalWrite(15, HIGH);
  digitalWrite(17, LOW);
}

void loop() {
  // put your main code here, to run repeatedly:
  while(digitalRead(15) == HIGH)
  { if(digitalRead(16) == LOW)
    {
      delay(300);
      digitalWrite(15, LOW);
      digitalWrite(17, HIGH);
    }    
  }
    while(digitalRead(17) == HIGH)
  { if(digitalRead(16) == LOW)
    {
      delay(300);
      digitalWrite(17, LOW);
      digitalWrite(14, HIGH);
    }    
  }
    while(digitalRead(14) == HIGH)
  { if(digitalRead(16) == LOW)
    {
      delay(300);
      digitalWrite(14, LOW);
      digitalWrite(15, HIGH);
    }    
  }
}
